﻿using System;
using System.Text;
using System.Web.UI;

namespace WebFormsLibrary
{
    public class PopUp : Control
    {
        public PopUp()
        {
            PopUnder = false;
            Resizable = false;
            Scrollbars = false;
            Url = "about:blank";
            WindowHeight = 300;
            WindowWidh = 300;
        }

        public bool PopUnder
        {
            get { return (bool)ViewState["PopUnder"]; }
            set { ViewState["PopUnder"] = value; }
        }

        public string Url
        {
            get { return (string)ViewState["Url"]; }
            set { ViewState["Url"] = value; }
        }

        public int WindowHeight
        {
            get { return (int)ViewState["WindowHeight"]; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Высота должна быть не менее 1 пикселя.");
                }

                ViewState["WindowHeight"] = value;
            }
        }

        public int WindowWidh
        {
            get { return (int)ViewState["WindowWidh"]; }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentException("Высота должна быть не менее 1 пикселя.");
                }

                ViewState["WindowWidh"] = value;
            }
        }

        public bool Resizable
        {
            get { return (bool)ViewState["Resizable"]; }
            set { ViewState["Resizable"] = value; }
        }

        public bool Scrollbars
        {
            get { return (bool)ViewState["Scrollbars"]; }
            set { ViewState["Scrollbars"] = value; }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (Page.Request == null || Page.Request.Browser.EcmaScriptVersion.Major >= 1)
            {
                StringBuilder jsStringBuilder = new StringBuilder();
                jsStringBuilder.Append("<script type='text/javascript'>");
                jsStringBuilder.Append("\nwindow.open(");
                jsStringBuilder.Append("'" + Url + "', '" + ID + "', 'toolbar=0,");
                jsStringBuilder.Append("height=" + WindowHeight + ",");
                jsStringBuilder.Append("width=" + WindowWidh + ",");
                jsStringBuilder.Append("resizable=" + Convert.ToInt16(Resizable).ToString() + ",");
                jsStringBuilder.Append("scrollbars=" + Convert.ToInt16(Scrollbars).ToString() + "');\n");
                if (PopUnder)
                {
                    jsStringBuilder.Append("window.focus();\n");
                }
                jsStringBuilder.Append("</script>\n");

                writer.Write(jsStringBuilder.ToString());
            }
            else
            {
                this.Page.Response.Write("Ваш браузер не поддерживает Javascript.");
            }
        }
    }
}
