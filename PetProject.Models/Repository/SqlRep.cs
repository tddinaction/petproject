﻿using Microsoft.Data.Entity;
using Microsoft.Data.Sqlite;
using System.Collections.Generic;
using System.Linq;
using PetProject.Models.Abstract;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;

namespace PetProject.Models.Repository
{
    public class SqlRep : DbContext
    {
        private static string dataSource;

        public SqlRep(string dataSourceString)
        {
            if (dataSourceString != null)
            {
                dataSource = dataSourceString;
            }
        }

        public SqlRep()
        {

        }

        //protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public DbSet<Owner> Owners { get; set; }
        public DbSet<Pet> Pets { get; set; }

        /// <summary>
        /// Метод сохранения, рекомендуется использовать из экземпляра класса DB.
        /// </summary>
        /// <returns></returns>
        public int SaveChanges()
        {
            return base.SaveChanges();
        }

        public void DisposeForDB()
        {
            base.Dispose();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder
            {
                DataSource = dataSource
            };
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);

            optionsBuilder.UseSqlite(connection);
        }

        public void InitializeDataBase()
        {
            new InitializerDb(this).Run();
        }

        public List<Owner> GetAllOwners()
        {
            return Owners.OrderBy(s => s.Name).ToList<Owner>();
        }

        public List<Pet> GetAllPetsByOwnerId(int oid)
        {
            return Pets.Where(s => s.OwnerId == oid).OrderBy(x => x.Name).ToList<Pet>();
        }

        public List<OwnerStatView> GetOwnerStatView()
        {
            List<OwnerStatView> result = new List<OwnerStatView>();

            foreach (Owner owner in GetAllOwners())
            {
                OwnerStatView item = new OwnerStatView()
                {
                    OwnerId = owner.OwnerId,
                    Name = owner.Name,
                    Count = GetAllPetsByOwnerId(owner.OwnerId).Count()
                };

                result.Add(item);
            }

            return result;
        }

        public List<PetStatView> GetPetStatView()
        {
            List<PetStatView> result = new List<PetStatView>();

            foreach (Pet pet in Pets.OrderBy(x => x.LastEditedAt).ToArray())
            {
                PetStatView item = new PetStatView()
                {
                    PetId = pet.PetId,
                    PetName = pet.Name,
                    OwnerName = Owners.SingleOrDefault(x => x.OwnerId == pet.OwnerId).Name,
                    ImagePath = pet.ImagePath,
                    PetLastEditedBy = pet.LastEditedBy,
                    PetLastEditedAt = pet.LastEditedAt.ToString("MM/dd/yyyy HH:mm"),
                    OwnerLastEditedBy = Owners.SingleOrDefault(x => x.OwnerId == pet.OwnerId).LastEditedBy,
                    OwnerLastEditedAt = Owners.SingleOrDefault(x => x.OwnerId == pet.OwnerId).LastEditedAt.ToString("MM/dd/yyyy HH:mm")
                };

                result.Add(item);
            }

            return result;
        }

        public Owner FindOwner(int oid)
        {
            Owner result = null;

            try
            {
                result = Owners.SingleOrDefault(m => m.OwnerId == oid);
            }
            catch
            {
                result = Owners.FirstOrDefault(m => m.OwnerId == oid);
            }

            return result;
        }

        public Pet FindPet(int id)
        {
            Pet result = null;

            try
            {
                result = Pets.SingleOrDefault(m => m.PetId == id);
            }
            catch
            {
                result = Pets.FirstOrDefault(m => m.PetId == id);
            }

            return result;
        }
    }
}