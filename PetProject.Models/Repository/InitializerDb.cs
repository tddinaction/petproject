﻿using PetProject.Models.Entities;
using System.Linq;

namespace PetProject.Models.Repository
{
    public class InitializerDb
    {
        private SqlRep Context { get; set; }

        public InitializerDb(SqlRep context)
        {
            Context = context;
        }

        /// <summary>
        /// Метод для наполнения БД начальными тестовыми или справочными значениями
        /// </summary>
        public void Run()
        {
            // Пример заполнения справочника:

            //if (Context.Owners.Count() == 0)
            //{
            //    Owner owner = new Owner()
            //    {
            //        Name = "Vika"
            //    };

            //    Context.Owners.Add(owner);
            //    Context.SaveChanges();
            //}

            //if (Context.Pets.Count() == 0)
            //{
            //    Pet pet = new Pet()
            //    {
            //        OwnerId = 1,
            //        Name = "Vaf-vaf"
            //    };

            //    Context.Pets.Add(pet);
            //    Context.SaveChanges();
            //}
        }
    }
}