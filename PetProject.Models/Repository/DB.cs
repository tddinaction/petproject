﻿using Ninject;
using PetProject.Models.Abstract;
using PetProject.Models.Concrete;
using System;

namespace PetProject.Models.Repository
{
    public class DB : IDisposable
    {
        public DB()
        {
             ChangeInstanceImplementation<DBRepository>();
        }

        public void ChangeInstanceImplementation<T_DBRepository>() where T_DBRepository : IDBRepository
        {
            IKernel ninjectKernel = new StandardKernel();
            ninjectKernel.Bind<IDBRepository>().To<T_DBRepository>();
            InjectedContext = ninjectKernel.Get<IDBRepository>();
        }

        /// <summary>
        /// Метод доступа к контесту БД, рекомендуемый для сохранения изменений и там где нет инцекций контества БД в контроллер
        /// </summary>
        public IDBRepository InjectedContext { get; set; }

        public void Dispose()
        {
            InjectedContext.DisposeForDB();
        }
    }
}