﻿using PetProject.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;
using PetProject.Models.Repository;

namespace PetProject.Models.Concrete
{
    public class DBRepository : IDBRepository
    {
        SqlRep sqlRep = new SqlRep();

        public IEnumerable<Owner> Owners
        {
            get { return sqlRep.Owners; }
        }

        public void OwnersDoAdd(Owner owner)
        {
            sqlRep.Owners.Add(owner);
        }

        public void OwnersDoRemove(Owner owner)
        {
            sqlRep.Owners.Remove(owner);
        }

        public IEnumerable<Pet> Pets
        {
            get { return sqlRep.Pets; }
        }

        public void PetsDoAdd(Pet pet)
        {
            sqlRep.Pets.Add(pet);
        }

        public void PetsDoRemove(Pet pet)
        {
            sqlRep.Pets.Remove(pet);
        }

        public void DisposeForDB()
        {
            sqlRep.DisposeForDB();
        }

        public Owner FindOwner(int oid)
        {
            return sqlRep.FindOwner(oid);
        }

        public Pet FindPet(int id)
        {
            return sqlRep.FindPet(id);
        }

        public List<Owner> GetAllOwners()
        {
            return sqlRep.GetAllOwners();
        }

        public List<Pet> GetAllPetsByOwnerId(int oid)
        {
            return sqlRep.GetAllPetsByOwnerId(oid);
        }

        public List<OwnerStatView> GetOwnerStatView()
        {
            return sqlRep.GetOwnerStatView();
        }

        public List<PetStatView> GetPetStatView()
        {
            return sqlRep.GetPetStatView();
        }

        public int SaveChanges()
        {
            return sqlRep.SaveChanges();
        }
    }
}
