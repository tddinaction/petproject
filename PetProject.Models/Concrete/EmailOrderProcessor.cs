﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PetProject.Models.Abstract;
using PetProject.Models.NoSqlEntities;
using System.Net.Mail;
using System.Net;

namespace PetProject.Models.Concrete
{
    public class EmailOrderProcessor : IOrderProcessor
    {
        public class EmailSettings
        {
            public string MailToAddress = "store@example.com";
            public string MailFromAddress = "site@example.com";
            public bool EnableSsl = true;
            public string Username = "Site";
            public string Password = "********";
            public string Host = "smtp@example.com";
            public int Port = 587;
            public bool WriteAsFile = true;
            public string FileLocation = AppDomain.CurrentDomain.GetData("DirectoryLettersToSend").ToString();
        }

        private EmailSettings emailSettings;

        public EmailOrderProcessor(EmailSettings settings)
        {
            emailSettings = settings;
        }

        public void SendMailMessage(string bodyText)
        {
            using (var smtpClient = new SmtpClient())
            {
                smtpClient.EnableSsl = emailSettings.EnableSsl;
                smtpClient.Host = emailSettings.Host;
                smtpClient.Port = emailSettings.Port;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential(emailSettings.Username, emailSettings.Password);

                if (emailSettings.WriteAsFile)
                {
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    smtpClient.PickupDirectoryLocation = emailSettings.FileLocation;
                    smtpClient.EnableSsl = false;
                }

                MailMessage mailMessage = new MailMessage(
                    emailSettings.MailFromAddress,
                    emailSettings.MailToAddress,
                    "Processing of sending invitations to the exhibition",
                    bodyText);

                if (emailSettings.WriteAsFile)
                {
                    mailMessage.BodyEncoding = Encoding.UTF8;
                }

                smtpClient.Send(mailMessage);
            }
        }

        public void ProcessorOrder(Cart cart, ShippingDetails shippingDetails)
        {
            SendMailMessage(GetBodyText(cart, shippingDetails));
        }

        private string GetBodyText(Cart cart, ShippingDetails shippingDetails)
        {
            StringBuilder body = new StringBuilder();
            body.AppendLine("Hi, dear content manager. I am going to organize the exhibition at " + shippingDetails.BeginsAt + ".")
                .AppendLine("Please agree on the presence of these pets at the forthcoming exhibition:");
            foreach (var cartLine in cart.Lines)
            {
                body.AppendLine("- Owner's name: " + cartLine.Pet.Name + ", Id: " + cartLine.Pet.PetId + ";");

            }
            body.AppendLine("With regard, " + shippingDetails.Name);
            return body.ToString();
        }
    }
}
