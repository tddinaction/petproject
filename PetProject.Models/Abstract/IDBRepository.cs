﻿using Microsoft.Data.Entity;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;
using System.Collections.Generic;

namespace PetProject.Models.Abstract
{
    public interface IDBRepository
    {
        IEnumerable<Owner> Owners { get; }
        void OwnersDoAdd(Owner owner);
        void OwnersDoRemove(Owner owner);
        IEnumerable<Pet> Pets { get; }
        void PetsDoAdd(Pet pet);
        void PetsDoRemove(Pet pet);
        List<Owner> GetAllOwners();
        List<Pet> GetAllPetsByOwnerId(int oid);
        List<OwnerStatView> GetOwnerStatView();
        List<PetStatView> GetPetStatView();
        Owner FindOwner(int oid);
        Pet FindPet(int id);

        int SaveChanges();
        void DisposeForDB();
    }
}