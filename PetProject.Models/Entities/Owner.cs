﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Serialization;
using System;

namespace PetProject.Models.Entities
{
    [DataContract]
    public class Owner
    {
        [DataMember]
        public int OwnerId { get; set; }

        [DataMember]
        [DisplayName("Name")]
        [Required]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9- ]+$", ErrorMessage = "Use only english characters, digits or hyphen.")]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Length must be between 1 and 255 characters.")]
        public string Name { get; set; }

        [DisplayName("Last edited by")]
        [DataType(DataType.Text)]
        public string LastEditedBy { get; set; }

        [DisplayName("Last edited at (UTC)")]
        [DataType(DataType.DateTime)]
        public DateTime LastEditedAt { get; set; }

        // пока не используем ссылочную целостность - она реализована программно
        public List<Pet> Pets { get; set; }
    }
}