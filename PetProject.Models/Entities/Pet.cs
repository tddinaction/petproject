﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Runtime.Serialization;
using PetProject.Infrastructure;
using System;

namespace PetProject.Models.Entities
{
    [DataContract]
    public class Pet
    {
        [DataMember]
        public int PetId { get; set; }

        [DataMember]
        [Required]
        [OwnerValidation] // custom validation attribute
        public int OwnerId { get; set; }

        [DataMember]
        [DisplayName("Name")]
        [Required]
        [DataType(DataType.Text)]
        [RegularExpression(@"^[a-zA-Z0-9- ]+$", ErrorMessage = "Use only english characters, digits or hyphen.")]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Length must be between 1 and 255 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Цена участия питомца в выставке
        /// </summary>
        [DisplayName("PriceExhibition")]
        [Required]
        public decimal PriceExhibition { get; set; } = 0;

        [DisplayName("Photo")]
        [DataType(DataType.Text)]
        [StringLength(1024)]
        public string ImagePath { get; set; }

        [DisplayName("Last edited by")]
        [DataType(DataType.Text)]
        public string LastEditedBy { get; set; }

        [DisplayName("Last edited at (UTC)")]
        [DataType(DataType.DateTime)]
        public DateTime LastEditedAt { get; set; }

        // пока не используем ссылочную целостность - она реализована программно
        public Owner Owner { get; set; }
    }
}