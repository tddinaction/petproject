﻿using System;

namespace PetProject.Models.NoSqlEntities
{
    public class PagingInfo
    {
        // общее количество
        public int TotalItems { get; set; }
        // количество на одной странице
        public int ItemsPerPage { get; set; }
        // номер текущей страницы
        public int CurrentPage { get; set; }
        // общее количество страниц
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }
    }
}