﻿using System.Collections.Generic;
using PetProject.Models.Entities;

namespace PetProject.Models.NoSqlEntities
{
    public class OwnerListViewModel
    {
        public IEnumerable<OwnerStatView> Owners { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}
