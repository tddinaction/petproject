﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace PetProject.Models.NoSqlEntities
{
    public class LoginData
    {

        [DisplayName("Login")]
        [Required(ErrorMessage = "Please enter your username.")]
        public string UserName { get; set; }



        [DisplayName("Password")]
        [Required(ErrorMessage = "Please enter your password.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}