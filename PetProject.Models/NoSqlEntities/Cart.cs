﻿using PetProject.Models.Entities;
using System.Collections.Generic;
using System.Linq;

namespace PetProject.Models.NoSqlEntities
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(Pet pet, int quantity)
        {
            // Проверку валидности pet выполнять в вызывающем методе действия контролера корзины

            CartLine line = lineCollection.Where(p => p.Pet.PetId == pet.PetId).FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Pet = pet,
                    Quantity = 1 // quantity - но пока все питомцы уникальны, поэтому 1
                });
            }
            else
            {
                // в случае реализации для интернет магазина нужно суммировать товар
                //line.Quantity += quantity;

                // но сейчас у нас, есть только приглашенный питомцы и все они уникальны
                line.Quantity = 1;
            }
        }

        public void RemoveLine(Pet pet)
        {
            lineCollection.RemoveAll(l => l.Pet.PetId == pet.PetId);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Pet.PriceExhibition * e.Quantity);
        }

        public int ComputeTotalQuantity()
        {
            return lineCollection.Sum(e => e.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

    public class CartLine
    {
        public Pet Pet { get; set; }
        public int Quantity { get; set; }
    }
}
