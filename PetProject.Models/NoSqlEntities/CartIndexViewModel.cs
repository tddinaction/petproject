﻿namespace PetProject.Models.NoSqlEntities
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string returnUrl { get; set; }
    }
}