﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetProject.Models.NoSqlEntities
{
    public class PetStatView
    {
        public int PetId { get; set; }

        [DisplayName("Pet`s name")]
        public string PetName { get; set; }

        [DisplayName("Owner`s name")]
        public string OwnerName { get; set; }

        public string ImagePath { get; set; }

        [DisplayName("Pet was last edited by")]
        public string PetLastEditedBy { get; set; }

        [DisplayName("Pet was last edited at (UTC)")]
        public string PetLastEditedAt { get; set; }

        [DisplayName("Owner was last edited by")]
        public string OwnerLastEditedBy { get; set; }

        [DisplayName("Owner was last edited at (UTC)")]
        public string OwnerLastEditedAt { get; set; }
    }
}
