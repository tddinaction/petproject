﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PetProject.Models.NoSqlEntities
{
    [DataContract]
    public class OwnerStatView
    {
        [DataMember]
        public int OwnerId { get; set; }
        [DisplayName("Name")]
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        [DisplayName("Pets Count")]
        public int Count { get; set; }
    }
}
