﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetProject.Models.NoSqlEntities
{
    public class ShippingDetails
    {
        [Required(ErrorMessage = "Please specify your name")]
        [RegularExpression(@"^[a-zA-Z0-9- ]+$", ErrorMessage = "Use only english characters, digits or hyphen.")]
        [Display(Name= "Your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please specify contact email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", ErrorMessage = "Mailing address is not valid")]
        [Display(Name = "Your contact email")]
        public string ContactEmail { get; set; }

        [Display(Name = "Start date and time")]
        [DataType(DataType.DateTime), DisplayFormat(DataFormatString = "{MM/dd/yyyy HH:mm}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Please enter the start date and time.")]
        public DateTime BeginsAt { get; set; }
    }
}
