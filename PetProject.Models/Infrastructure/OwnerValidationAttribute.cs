﻿using PetProject.Models.Repository;
using System.ComponentModel.DataAnnotations;

namespace PetProject.Infrastructure
{
    public class OwnerValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            bool noMistakes = true;

            try
            {
                using (var db = new DB())
                {
                    if (db.InjectedContext.FindOwner((int)value) == null) noMistakes = false;
                }
            }
            catch
            {
                noMistakes = false;
            }

            return noMistakes;
        }
    }
}