﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CallWebForm.aspx.cs" Inherits="PetProject.CallWebForm" %>
<%@ Register Assembly="WebFormsLibrary" Namespace="WebFormsLibrary" TagPrefix="webFormsLibrary" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta charset="utf-8" />
    <title>WebForms</title>
    <style>
        .yellowstone {
            background-color:antiquewhite;
        }
        .hider {
            visibility: hidden;
        }
        .titletext {
            font-size: x-large; 
            font-weight: bold;
        }
    </style>
    <script src="Scripts/jquery-1.12.4.min.js"></script>
            <script>
                var xmlRequest;
                var CurrOnwerName = "";
                var iLoopCounter = 1;
                var iMaxLoop = 40;
                var iIntervalId;

                function PageBeginLoad() {
                    CreateXmlRequest()
                    iIntervalId = window.setInterval("iLoopCounter=UpdateProgressMeter(iLoopCounter, iMaxLoop)", 200);
                }

                function UpdateProgressMeter(iCurrLoopCounter, iMaxumLoop) {

                    if (!$("#TextBoxAsk").hasClass("yellowstone")) {
                        $("#insSpan").toggleClass("hider");
                        $("#ArrowToDown").toggleClass("hider");
                    }
                    else
                    {
                        $("#insSpan").removeClass("hider");
                        $("#ArrowToDown").addClass("hider");
                    }

                    var progressMeter = document.getElementById("ProgressMeter");
                    var report = document.getElementById("Report");

                iCurrLoopCounter += 1;
                if (iCurrLoopCounter <= iMaxumLoop) {
                    report.innerHTML = " (" + iCurrLoopCounter + "/" + iMaxLoop + ") ";
                    progressMeter.innerHTML += "&#10003;";
                    return iCurrLoopCounter;
                }
                else {
                    progressMeter.innerHTML = "";
                    report.innerHTML = "";
                    PageEndLoad();
                    return 1;
                }
            }

            function PageEndLoad() {
                window.clearInterval(iIntervalId);
                                
                document.getElementById("Title").innerHTML = "Returning...";

                setTimeout(GoBack, 500);
                }

                function GoBack() {
                    window.location.href = "<%=PageToLoad %>";
                    window.location.assign();
                }

                function CreateXmlRequest() {
                    try
                    {
                        xmlRequest = new XMLHttpRequest();
                    }
                    catch (err)
                    {
                        xmlRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                }

                function AskHandler() {
                    var textBoxAsk = document.getElementById("TextBoxAsk");

                    if (textBoxAsk.value != CurrOnwerName)
                    {
                        CurrOnwerName = textBoxAsk.value;

                        if (CurrOnwerName.length > 0)
                        {
                            var url = "AjaxAnswerPetsNameByOwnerNameHandler.ashx?OwnerName=" + CurrOnwerName;
                            xmlRequest.open("GET", url);
                            xmlRequest.onreadystatechange = ShowAnswer;
                            xmlRequest.send(null);
                        }
                    }
                }

                function ShowAnswer() {
                    if (xmlRequest.readyState == 4)
                    {
                        if (xmlRequest.status == 200)
                        {
                            var labelAnswer = document.getElementById("LabelAnswer");

                            labelAnswer.innerText = xmlRequest.responseText;
                        }
                    }
                }

                $(document).ready(function () {
                    $("#TextBoxAsk").mouseenter(function () { $(this).addClass("yellowstone"); $("#ArrowToLeft").append("&#9997;"); ; $(this).unbind("mouseenter"); });
                });
    </script>
</head>
    <body  onload="PageBeginLoad()">
        <form runat="server">
        <div style="margin-left: 25px; margin-top: 25px">
            <span id="Title" class="titletext">Adding signs</span><span id="Report" class="titletext"></span><span id="ProgressMeter" class="titletext"></span>
            <p style="margin-left: 50px; margin-top:25px"><img src="Content/img/ajax-loader.gif" /></p>
            <p><span id="insSpan">Input name of the owner: </span><span id="ArrowToDown">&#8681;</span></p>
            <asp:TextBox ID="TextBoxAsk" runat="server" Width="200" onKeyUp="AskHandler()" autocomplete = "off"></asp:TextBox><span id="ArrowToLeft"></span>
            <p style="margin-top: 25px"><asp:Label ID="LabelAnswer" runat="server"></asp:Label></p>
        </div>
        </form>
    </body>
    <webFormsLibrary:PopUp ID="PopUpSart" runat="server" Url="https://bitbucket.org/tddinaction/petproject/src"></webFormsLibrary:PopUp>
</html>




