﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PetProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "DefaultCore",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                name: "DefaultAccount",
                url: "Account/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultHome",
                url: "Home/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultDetail",
                url: "Detail/{action}/{id}",
                defaults: new { controller = "Detail", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultSlider",
                url: "Slider/{action}/{id}",
                defaults: new { controller = "Slider", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultNav",
                url: "Nav/{action}/{id}",
                defaults: new { controller = "Nav", action = "OwnerMenu", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultCart",
                url: "Cart/{action}/{id}",
                defaults: new { controller = "Cart", action = "Summary", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "DefaultUtils",
                url: "Utils/{action}/{id}",
                defaults: new { controller = "Utils", action = "MessageBox", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "FasterResponse",
                url: "Fastest",
                defaults: new { controller = "Fastest", action = "Index" }
            );

            routes.MapRoute(
                name: "CatchAll",
                url: "{*url}",
                defaults: new { controller = "Error", action = "Index" }
            );

        }
        private class OnlyExistingCategoriesConstraint : IRouteConstraint
        {
            public bool Match
                (
                    HttpContextBase httpContext,
                    Route route,
                    string parameterName,
                    RouteValueDictionary values,
                    RouteDirection routeDirection
                )
            {
                var category = route.DataTokens["category"];
                //TODO: Look it up in your database etc


                // fake that the category exists
                return true;
            }
        }
    }
}

