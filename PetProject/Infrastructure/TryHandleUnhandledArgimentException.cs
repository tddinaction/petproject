﻿using System;
using System.Web.Mvc;

namespace PetProject.Infrastructure
{
    public class TryHandleUnhandledArgimentExceptionAttribute : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Здесь будем перехватывать необработанные исключения контроллеров MVC
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                if (filterContext.Exception is ArgumentOutOfRangeException)
                {
                    filterContext.Result = new RedirectResult("~/Error/Index");
                    filterContext.ExceptionHandled = true;
                }
                // else if ...
            }
        }
    }
}
