﻿using PetProject.Infrastructure;
using PetProject.Models.NoSqlEntities;
using System.Web.Security;


namespace PetProject
{
    public class FormsAuthenticationProvider : IFormsAuthenticationProvider
    {
        public bool Authenticate(LoginData loginData)
        {
            bool authResult = FormsAuthentication.Authenticate(loginData.UserName, loginData.Password);

            if (authResult)
            {
                FormsAuthentication.SetAuthCookie(loginData.UserName, false);
            }

            return authResult;
        }

        public void Logout()
        {
            FormsAuthentication.SignOut();
        }
    }
}