﻿using System.Web;

namespace PetProject.Infrastructure
{
    public class RemoteMvcAuthorizeAttribute : System.Web.Mvc.AuthorizeAttribute
    {
        /// <summary>
        /// Переопределили, чтобы авторизация требовалась только для удалённых пользователей в MVC
        /// </summary>
        /// <param name="httpContext"></param>
        /// <returns></returns>
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (httpContext.Request.IsLocal)
            {
                return httpContext.Request.IsAuthenticated; // установить "true", чтобы отменить авторизацию для локальных пользователей 
            }
            else
            {
                return httpContext.Request.IsAuthenticated;
            }
        }
    }
}