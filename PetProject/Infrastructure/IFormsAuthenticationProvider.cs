﻿using PetProject.Models.NoSqlEntities;

namespace PetProject.Infrastructure
{
    public interface IFormsAuthenticationProvider
    {
        bool Authenticate(LoginData loginData);
        void Logout();
    }
}