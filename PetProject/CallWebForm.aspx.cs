﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Threading;

namespace PetProject
{
    public partial class CallWebForm : System.Web.UI.Page
    {
        protected string PageToLoad;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.UrlReferrer != null)
                {
                    PageToLoad = Request.UrlReferrer.ToString();
                }
                else
                {
                    PageToLoad = "Home/Index";
                }
            }
        }
    }
}