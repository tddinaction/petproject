﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PetProject.Models.NoSqlEntities;

namespace PetProject.Controllers
{
    public class AjaxHomeController : BaseApiController
    {
        // Реализовывал только через метод GET чтобы была возможность тестировать через броузер и встроенный IIS

        [HttpGet]
        public IEnumerable<OwnerStatView> Index()
        {
            List<OwnerStatView> ownersInfo = new List<OwnerStatView>();

            try
            {
                ownersInfo = InjectedDBRepository.GetOwnerStatView();
            }
            catch { }

            return ownersInfo;
        }

        #region <Create>


        [HttpGet]
        public HttpResponseMessage Create([FromUri]string name)
        {
            name = Utils.DeleteSpecialCharacters(name);

            try
            {
                if (ModelState.IsValid && !string.IsNullOrEmpty(name))
                {
                    using (var db = new DB())
                    {
                        Owner owner = new Owner();
                        owner.Name = name;
                        db.InjectedContext.OwnersDoAdd(owner);
                        db.InjectedContext.SaveChanges();
                    }

                    // случай запроса WCF
                    if (Request == null)
                    {
                        return new HttpResponseMessage { StatusCode = HttpStatusCode.Created };
                    }

                    var response = Request.CreateResponse(HttpStatusCode.Created, name);

                    string uri = Url.Link("DefaultApi", name);
                    response.Headers.Location = new Uri(uri);
                    return response;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                // случай запроса WCF
                if (Request == null)
                {
                    return new HttpResponseMessage { StatusCode = HttpStatusCode.ExpectationFailed };
                }

                var response = Request.CreateResponse(HttpStatusCode.ExpectationFailed);

                string uri = Url.Link("DefaultApi", 0);
                response.Headers.Location = new Uri(uri);
                return response;
            }

        }

        #endregion

        #region Delete

        [HttpGet]
        public HttpResponseMessage Delete([FromUri]int oid)
        {
            Owner owner = null;

            try
            {
                if (oid > 0)
                {
                    using (var db = new DB())
                    {
                        owner = db.InjectedContext.FindOwner(oid);

                        if (owner != null)
                        {
                            List<Pet> dropPetList = db.InjectedContext.Pets.Where(s => s.OwnerId == oid).ToList<Pet>();
                            foreach (var dropPet in dropPetList)
                            {
                                db.InjectedContext.PetsDoRemove(dropPet);
                            }

                            db.InjectedContext.OwnersDoRemove(owner);
                            db.InjectedContext.SaveChanges();

                            // случай запроса WCF
                            if (Request == null)
                            {
                                return new HttpResponseMessage { StatusCode = HttpStatusCode.Moved };
                            }

                            var response = Request.CreateResponse(HttpStatusCode.Moved, "Ok");

                            string uri = Url.Link("DefaultApi", "Ok");
                            response.Headers.Location = new Uri(uri);
                            return response;
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                // случай запроса WCF
                if (Request == null)
                {
                    return new HttpResponseMessage { StatusCode = HttpStatusCode.ExpectationFailed };
                }

                var response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, "Error");

                string uri = Url.Link("DefaultApi", "Error");
                response.Headers.Location = new Uri(uri);
                return response;
            }
        }

        #endregion


    }
}
