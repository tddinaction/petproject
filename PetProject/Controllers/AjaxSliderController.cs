﻿using PetProject.Models.NoSqlEntities;
using PetProject.Models.Repository;
using System.Collections.Generic;
using System.Web.Http;

namespace PetProject.Controllers
{
    public class AjaxSliderController : BaseApiController
    {
        [HttpGet]
        public IEnumerable<PetStatView> Index()
        {
            List<PetStatView> petStatView = new List<PetStatView>();

            try
            {
                petStatView = InjectedDBRepository.GetPetStatView();
            }
            catch { }

            return petStatView;
        }
    }
}
