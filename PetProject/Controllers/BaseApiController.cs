﻿using Ninject;
using PetProject.Models.Abstract;
using System.Web.Http;

namespace PetProject.Controllers
{
    [Authorize]
    public class BaseApiController : ApiController
    {
        /// <summary>
        /// Альтернативный вариант контекста с меньшим колв-ом кода и большей тестиремостью, 
        /// но потребляющий больше ресурсов сервера
        /// </summary>
        [Inject]
        public IDBRepository InjectedDBRepository { get; set; }

        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
    }
}