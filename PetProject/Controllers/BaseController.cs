﻿using System.Web.Mvc;
using Ninject;
using PetProject.Infrastructure;
using PetProject.Models.Abstract;
using Newtonsoft.Json;

namespace PetProject.Controllers
{
    [RemoteMvcAuthorize]
    [TryHandleUnhandledArgimentException]
    public class BaseController : Controller
    {
        /// <summary>
        /// Свойство определяет количество элементов на странице
        /// </summary>
        public int PageSize { get; set; } = 3;

        /// <summary>
        /// Альтернативный вариант контекста с меньшим колв-ом кода и большей тестиремостью, 
        /// но потребляющий больше ресурсов сервера
        /// </summary>
        // Атрибут [Inject] закоментирован, внедрение происходит непосредсвенно в кострукторах производных классов для повышения тестируемости
        public IDBRepository InjectedDBRepository { get; set; }

        protected static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void FillCommonViewBag()
        {
            try
            {
                ViewBag.CurrUser = HttpContext.User.Identity.Name;
            }
            catch { }
        }

        /// <summary>
        /// Сохраняет значение в кэш и возвращает ключ, по которому это значение можно достать.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        protected string StoreDataInCache(string data)
        {
            long t = System.DateTime.UtcNow.Ticks;
            string key = t.ToString();

            while (System.Web.HttpContext.Current.Cache[key] != null)
            {
                t++;
                key = t.ToString();
            }
            System.Web.HttpContext.Current.Cache[key] = data;

            return key;
        }

        /// <summary>
        /// Достаёт значение из кэша по ключу и удаляет значение из кэша.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        protected string GetDataFromCache(string key)
        {
            string data = System.Web.HttpContext.Current.Cache[key] as string;
            System.Web.HttpContext.Current.Cache.Remove(key);
            return data;
        }

        /// <summary>
        /// Подготавливает данные для вывода окна с указанным сообщением и заголовком.
        /// Использование: передать возвращённый данным методом ключ на страницу с помощью параметра notify
        /// Пример: Home/Index?notify=234234234234234
        /// </summary>
        /// <param name="_message"></param>
        /// <param name="_header"></param>
        /// <param name="_error"></param>
        /// <returns></returns>
        protected string PrepareWarningMessageBox(string _message = "Ошибка, повторите попытку заново.", bool _error = true, string _header = "Внимание!")
        {
            string result = null;
            var messageObject = new { header = _header, message = _message, error = _error };
            string messageJson = JsonConvert.SerializeObject(messageObject);

            try
            {
                result = StoreDataInCache(messageJson);
            } catch { }

            return result;
        }
    }
}