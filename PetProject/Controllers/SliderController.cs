﻿using PetProject.Models.Abstract;
using PetProject.Models.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace PetProject.Controllers
{
    public class SliderController : BaseController
    {
        public SliderController(IDBRepository repo)
        {
            InjectedDBRepository = repo;
        }

        public ActionResult Index()
        {
            FillCommonViewBag();

            List<Owner> listOwner = new List<Owner>();

            listOwner.Add( new Owner());

            try
            {
                listOwner.AddRange(InjectedDBRepository.GetAllOwners());
            }
            catch { }

            ViewBag.listOwner = listOwner;

            return View();
        }
    }
}
