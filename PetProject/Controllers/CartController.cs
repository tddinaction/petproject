﻿using PetProject.Models.Abstract;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;
using System;
using System.Linq;
using System.Web.Mvc;

namespace PetProject.Controllers
{
    public class CartController : BaseController
    {
        private IOrderProcessor orderProcessor;

        public CartController(IDBRepository repo, IOrderProcessor processor)
        {
            InjectedDBRepository = repo;
            orderProcessor = processor;
        }

        public ViewResult Index(Cart cart,string returnUrl)
        {
            FillCommonViewBag();

            return View(new CartIndexViewModel
            {
                Cart = cart,
                returnUrl = returnUrl
            });
        }

        public RedirectToRouteResult AddToCart(Cart cart, int PetId, string returnUrl)
        {
            FillCommonViewBag();

            Pet pet =null;

            try
            {
                pet = InjectedDBRepository.Pets.FirstOrDefault(g => g.PetId == PetId);
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
            }

            if (pet != null)
            {
                cart.AddItem(pet, 1);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int PetId, string returnUrl)
        {
            FillCommonViewBag();

            Pet pet = null;

            try
            {
                pet = InjectedDBRepository.Pets.FirstOrDefault(g => g.PetId == PetId);
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
            }

            if (pet != null)
            {
                cart.RemoveLine(pet);
            }

            return RedirectToAction("Index", new { returnUrl });
        }

        public PartialViewResult Summary(Cart cart, bool small = false)
        {
            FillCommonViewBag();

            return PartialView(cart);
        }

        public ViewResult Checkout()
        {
            FillCommonViewBag();

            return View(new ShippingDetails());
        }

        [HttpPost]
        public ViewResult Checkout(Cart cart, ShippingDetails shippingDetails)
        {
            FillCommonViewBag();

            if (cart.Lines.Count() == 0)
            {
                ModelState.AddModelError("", "Sorry, the cart is empty!");
            }

            if (ModelState.IsValid)
            {
                orderProcessor.ProcessorOrder(cart, shippingDetails);
                cart.Clear();
                return View("Complete");
            }
            else
            {
                return View(shippingDetails);
            }
        }

        public ViewResult Complete()
        {
            FillCommonViewBag();

            return View();
        }
    }
}