﻿using PetProject.Models.Abstract;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PetProject.Controllers
{
    public class NavController : BaseController
    {
        public NavController(IDBRepository repo)
        {
            InjectedDBRepository = repo;
        }

        [ChildActionOnly]
        public PartialViewResult OwnerMenu(int? oid = null)
        {
            ViewBag.SelectedOnwerId = oid ?? -1;

            List<Owner> owners = new List<Owner>();

            try
            {
                owners = InjectedDBRepository.Owners.OrderBy(x => x.Name).ToList();
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
            }

            return PartialView(owners);
        }
    }
}