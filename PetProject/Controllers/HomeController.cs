﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PetProject.Models.NoSqlEntities;
using PetProject.Models.Abstract;
using System.IO;

namespace PetProject.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IDBRepository repo)
        {
            InjectedDBRepository = repo;
        }

        [HttpGet]
        public ActionResult Index(int page = 1)
        {
            FillCommonViewBag();

            OwnerListViewModel ownersInfo;

            try
            {
                ownersInfo = new OwnerListViewModel
                {
                    Owners = InjectedDBRepository.GetOwnerStatView().Skip((page - 1) * PageSize).Take(PageSize),
                    PagingInfo = new PagingInfo
                    {
                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = InjectedDBRepository.GetOwnerStatView().Count
                    }
                };
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

            return View(ownersInfo);
        }

        #region <Create>

        [HttpGet]
        public ActionResult Create()
        {
            Owner owner = new Owner();

            return PartialView(owner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Owner owner)
        {
            try
            {
                owner.Name = Utils.DeleteSpecialCharacters(owner.Name);
                try { owner.LastEditedBy = HttpContext.User.Identity.Name; } catch { owner.LastEditedBy = "UnitTests"; }
                owner.LastEditedAt = DateTime.UtcNow;

                if (ModelState.IsValid)
                {
                    try
                    {
                        using (var db = new DB())
                        {
                            db.InjectedContext.OwnersDoAdd(owner);
                            db.InjectedContext.SaveChanges();
                        }

                        string msg = string.Format(@"{0} has been created.", owner.Name);
                        logger.Info(msg);
                        TempData["message"] = msg;
                        return RedirectToAction("Index", "Home");
                    }
                    catch 
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }
        }

        #endregion

        #region <Edit>

        public ActionResult Edit(int? oid)
        {
            Owner owner = null;

            try
            {
                if (oid != null)
                {
                     owner = InjectedDBRepository.FindOwner(oid ?? 0);
                }

                if (owner == null)
                {
                    throw new Exception();
                }

                try
                {
                    UpdateModel(owner, new FormValueProvider(ControllerContext));
                } catch { }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

            return PartialView(owner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Owner owner)
        {
            try
            {
                owner.Name = Utils.DeleteSpecialCharacters(owner.Name);
                try { owner.LastEditedBy = HttpContext.User.Identity.Name; } catch { owner.LastEditedBy = "UnitTests"; }
                owner.LastEditedAt = DateTime.UtcNow;

                if (ModelState.IsValid)
                {
                    using (var db = new DB())
                    {
                        var existOwner = db.InjectedContext.FindOwner(owner.OwnerId);
                        if (existOwner != null)
                        {
                            existOwner.Name = owner.Name;
                            existOwner.LastEditedBy = existOwner.LastEditedBy;
                            existOwner.LastEditedAt = existOwner.LastEditedAt;

                            db.InjectedContext.SaveChanges();
                        }
                    }

                    string msg = string.Format(@"{0} has been edited.", owner.Name);
                    logger.Info(msg);
                    TempData["message"] = msg;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }
        }

        #endregion

        #region Delete

        [HttpGet]
        public ActionResult Delete(int? oid)
        {
            Owner owner = null;

            try
            {
                if (oid != null)
                {
                     owner = InjectedDBRepository.FindOwner(oid ?? 0);
                }

                if (owner == null)
                {
                    throw new Exception();
                }

                try
                {
                    UpdateModel(owner, new FormValueProvider(ControllerContext));
                }
                catch { }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

            return PartialView(owner);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Owner owner)
        {
            try
            {
                string ownerName = owner.Name;

                using (var db = new DB())
                {
                    List<Pet> dropPetList = db.InjectedContext.Pets.Where(s => s.OwnerId == owner.OwnerId).ToList<Pet>();
                    foreach(var dropPet in dropPetList)
                    {
                        try
                        {
                            DeletePetImageFile(dropPet.ImagePath);
                        }
                        catch { }

                        db.InjectedContext.PetsDoRemove(dropPet);
                    }

                    db.InjectedContext.OwnersDoRemove(owner);
                    db.InjectedContext.SaveChanges();
                }

                string msg = string.Format(@"{0} has been deleted.", ownerName);
                logger.Info(msg);
                TempData["message"] = msg;
                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }
        }

        #endregion

        #region UtilsWorkWithPetImageFile

        private void DeletePetImageFile(string imagePath)
        {
            string TargetLocation = Server.MapPath("~/Content/PetImages");

            FileInfo fi = new FileInfo(imagePath);
            string resultFileName = Path.Combine(TargetLocation, fi.Name);

            if (System.IO.File.Exists(resultFileName))
            {
                System.IO.File.Delete(resultFileName);
            }
        }

        #endregion
    }

}
