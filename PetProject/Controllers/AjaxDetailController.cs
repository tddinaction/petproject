﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PetProject.Controllers
{
    public class AjaxDetailController : BaseApiController
    {
        // Реализовывал только через метод GET чтобы была возможность тестировать через броузер и встроенный IIS

        [HttpGet]
        public IEnumerable<Pet> Index([FromUri]int oid)
        {
            List<Pet> listPet = new List<Pet>();

            try
            {
                listPet = InjectedDBRepository.GetAllPetsByOwnerId(oid);

            }
            catch { }

            return listPet;
        }


        #region <Create>


        [HttpGet]
        public HttpResponseMessage Create([FromUri]string name, [FromUri]int oid)
        {
            name = Utils.DeleteSpecialCharacters(name);

            try
            {
                if (ModelState.IsValid && !string.IsNullOrEmpty(name) && oid > 0)
                {
                    using (var db = new DB())
                    {
                        if (db.InjectedContext.FindOwner(oid) == null)
                        {
                            throw new Exception();
                        }

                        Pet pet = new Pet()
                        {
                            Name = name,
                            OwnerId = oid
                        };

                        db.InjectedContext.PetsDoAdd(pet);
                        db.InjectedContext.SaveChanges();
                    }

                    // случай запроса WCF
                    if (Request == null)
                    {
                        return new HttpResponseMessage { StatusCode = HttpStatusCode.Created };
                    }

                    var response = Request.CreateResponse(HttpStatusCode.Created, name);

                    string uri = Url.Link("DefaultApi", name);
                    response.Headers.Location = new Uri(uri);
                    return response;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch
            {
                // случай запроса WCF
                if (Request == null)
                {
                    return new HttpResponseMessage { StatusCode = HttpStatusCode.ExpectationFailed };
                }

                var response = Request.CreateResponse(HttpStatusCode.ExpectationFailed);

                string uri = Url.Link("DefaultApi", 0);
                response.Headers.Location = new Uri(uri);
                return response;
            }
        }


        #endregion

        #region Delete

        [HttpGet]
        public HttpResponseMessage Delete([FromUri]int pid, [FromUri]int oid)
        {
            // oid просто для определения этого метода перегрузки

            Pet pet = null;
            int oldPetId = pid;

            try
            {
                if (pid > 0)
                {
                    using (var db = new DB())
                    {
                        pet = db.InjectedContext.FindPet(pid);

                        if (pet != null)
                        {
                            if (pet.OwnerId != oid)
                            {
                                throw new Exception();
                            }

                            db.InjectedContext.PetsDoRemove(pet);
                            db.InjectedContext.SaveChanges();

                            // случай запроса WCF
                            if (Request == null)
                            {
                                return new HttpResponseMessage { StatusCode = HttpStatusCode.Moved };
                            }

                            var response = Request.CreateResponse(HttpStatusCode.Moved, "Ok");

                            string uri = Url.Link("DefaultApi", "Ok");
                            response.Headers.Location = new Uri(uri);
                            return response;
                        }
                        else
                        {
                            throw new Exception();
                        }
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception)
            {
                // случай запроса WCF
                if (Request == null)
                {
                    return new HttpResponseMessage { StatusCode = HttpStatusCode.ExpectationFailed };
                }

                var response = Request.CreateResponse(HttpStatusCode.ExpectationFailed, "Eror");

                string uri = Url.Link("DefaultApi", "Error");
                response.Headers.Location = new Uri(uri);
                return response;
            }
        }

        #endregion

    }
}
