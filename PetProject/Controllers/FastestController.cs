﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace PetProject.Controllers
{
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class FastestController : IController
    {
        public void Execute(RequestContext requestContext)
        {
            requestContext.HttpContext.Response.Write("Here could be your data delivered with the fastest response rate (" + (DateTime.Now - requestContext.HttpContext.Timestamp).TotalMilliseconds + " милисекунд).");
        }
    }
}