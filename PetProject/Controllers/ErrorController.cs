﻿using System.Web.Mvc;

namespace PetProject.Controllers
{
    public class ErrorController : BaseController
    {
        [AllowAnonymous]
        [OutputCache(Duration = 86400)]
        public ActionResult Index()
        {
            return View();
        }
    }
}