﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using PetProject.Models.Abstract;
using PetProject.Models.NoSqlEntities;

namespace PetProject.Controllers
{
    public class DetailController : BaseController
    {
        public DetailController(IDBRepository repo)
        {
            InjectedDBRepository = repo;
        }

        // GET: Detail
        public ActionResult Index(int? oid)
        {
            FillCommonViewBag();

            List<Pet> listPet = new List<Pet>();

            try
            {
                if (oid == null || oid == 0)
                {
                    throw new Exception();
                }

                listPet = InjectedDBRepository.GetAllPetsByOwnerId((int)oid);
                ViewBag.OwnerId = (int)oid;
                ViewBag.OwnerName = InjectedDBRepository.FindOwner((int)oid).Name;
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

                return View(listPet);
        }

        public ActionResult PetSummary(int? pid, string returnUrl)
        {
            FillCommonViewBag();

            ViewBag.ReturnUrl = returnUrl;

            Pet pet = null;

            try
            {
                if (pid == null || returnUrl == null)
                {
                    throw new Exception();
                }

                pet = InjectedDBRepository.FindPet(pid ?? 0);

                if (pet == null)
                {
                    throw new Exception();
                }
            }

            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

            return View(pet);
        }

        #region <Create>

        [HttpGet]
        public ActionResult Create(int? oid)
        {
            try
            {
                if (oid == null || oid == 0)
                {
                    throw new Exception();
                }
                else
                {
                    if (InjectedDBRepository.FindOwner(oid ?? 0) == null)
                    {
                        throw new Exception();
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });

            }

            Pet pet = new Pet();

            pet.OwnerId = (int)oid;

            return PartialView(pet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Pet pet)
        {
            try
            {
                pet.Name = Utils.DeleteSpecialCharacters(pet.Name);
                try { pet.LastEditedBy = HttpContext.User.Identity.Name; } catch { pet.LastEditedBy = "UnitTests"; }
                pet.LastEditedAt = DateTime.UtcNow;

                if (ModelState.IsValid)
                {
                    try
                    {
                        using (var db = new DB())
                        {
                            string petImagePath = SavePetImageFile();

                            if (!string.IsNullOrEmpty(petImagePath))
                            {
                                pet.ImagePath = petImagePath;
                            }

                            db.InjectedContext.PetsDoAdd(pet);
                            db.InjectedContext.SaveChanges();
                        }

                        string msg = string.Format(@"{0} has been created.", pet.Name);
                        logger.Info(msg);
                        TempData["message"] = msg;
                        return RedirectToAction("Index", "Detail", new { oid = pet.OwnerId });
                    }
                    catch (Exception e)
                    {
                        throw new Exception();
                    }
                }
                else
                {
                    throw new Exception();
                }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Detail", new { notify = PrepareWarningMessageBox() });
            }
        }
        #endregion

        #region <Edit>

        public ActionResult Edit(int? pid)
        {
            Pet pet = null;

            try
            {
                if (pid != null)
                {
                    pet = InjectedDBRepository.FindPet(pid ?? 0);
                }

                if (pet == null)
                {
                    throw new Exception();
                }

                try
                {
                    UpdateModel(pet, new FormValueProvider(ControllerContext));
                }
                catch { }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }

            return PartialView(pet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Pet pet)
        {
            try
            {
                pet.Name = Utils.DeleteSpecialCharacters(pet.Name);
                try { pet.LastEditedBy = HttpContext.User.Identity.Name; } catch { pet.LastEditedBy = "UnitTests"; }
                pet.LastEditedAt = DateTime.UtcNow;

                if (ModelState.IsValid)
                {
                    using (var db = new DB())
                    {
                        var existPet = db.InjectedContext.FindPet(pet.PetId);
                        if (existPet != null)
                        {
                            existPet.Name = pet.Name;
                            existPet.ImagePath = EditPetImageFile(existPet.ImagePath, pet.ImagePath);
                            existPet.LastEditedBy = pet.LastEditedBy;
                            existPet.LastEditedAt = pet.LastEditedAt;

                            db.InjectedContext.SaveChanges();
                        }
                    }

                    string msg = string.Format(@"{0} has been edited.", pet.Name);
                    logger.Info(msg);
                    TempData["message"] = msg;
                    return RedirectToAction("Index", "Detail", new { oid = pet.OwnerId });
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Home", new { notify = PrepareWarningMessageBox() });
            }
        }

        #endregion

        #region Delete

        [HttpGet]
        public ActionResult Delete(int? pid)
        {
            Pet pet = new Pet();

            try
            {
                if (pid == null || pid == 0)
                {
                    throw new Exception();
                }

                pet = InjectedDBRepository.FindPet(pid ?? 0);


                if (pet == null)
                {
                    throw new Exception();
                }

                try
                {
                    UpdateModel(pet, new FormValueProvider(ControllerContext));
                }
                catch { }
            }
            catch(Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Detail", new { notify = PrepareWarningMessageBox() });
            }

            return PartialView(pet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(Pet pet, Cart cart)
        {
            try
            {
                string petName = pet.Name;

                using (var db = new DB())
                {
                    try
                    {
                        cart.RemoveLine(pet);
                    }
                    catch { }

                    try
                    {
                        if (!string.IsNullOrEmpty(pet.ImagePath))
                        {
                            DeletePetImageFile(pet.ImagePath);
                        }
                    }
                    catch { }

                    db.InjectedContext.PetsDoRemove(pet);
                    db.InjectedContext.SaveChanges();
                }

                string msg = string.Format(@"{0} has been deleted.", petName);
                logger.Info(msg);
                TempData["message"] = msg;
                return RedirectToAction("Index", "Detail", new { oid = pet.OwnerId });
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                return RedirectToAction("Index", "Detail", new { notify = PrepareWarningMessageBox() });
            }
        }

        #endregion

        #region UtilsWorkWithPetImageFile

        private string SavePetImageFile(string autoFileName = null)
        {
            HttpPostedFileBase myFile = HttpContext.Request.Files["ImagePath"];
            string targetLocation = Server.MapPath("~/Content/PetImages");
            if (autoFileName == null)
            {
                autoFileName = DateTime.UtcNow.Ticks.ToString();
            }

            string resultFileName = null;

            if (myFile != null)
            {
                if (myFile.ContentLength == 0) return null;

                FileInfo fi = new FileInfo(myFile.FileName);
                string ext = fi.Extension.ToLower();
                if (ext != ".jpeg" && ext != ".jpg" && ext != ".png")
                {
                    return null;
                }

                autoFileName = autoFileName + ext;
                resultFileName = Path.Combine(targetLocation, autoFileName);

                if (myFile.ContentLength > 0)
                {
                    //загружаем содержимое файла
                    int fileSize = myFile.ContentLength;
                    byte[] FileByteArray = new byte[fileSize];
                    myFile.InputStream.Read(FileByteArray, 0, fileSize);

                    //сохраняем файл
                    using (FileStream fs = new FileStream(resultFileName, FileMode.Create))
                    {
                        fs.Write(FileByteArray, 0, fileSize);
                    }
                }
                //return resultFileName;
                return "/Content/PetImages/" + autoFileName;
            }
            return null;
        }

        private void DeletePetImageFile(string imagePath)
        {
            string TargetLocation = Server.MapPath("~/Content/PetImages");

            FileInfo fi = new FileInfo(imagePath);
            string resultFileName = Path.Combine(TargetLocation, fi.Name);

            if (System.IO.File.Exists(resultFileName))
            {
                System.IO.File.Delete(resultFileName);
            }
        }

        private string EditPetImageFile(string existPetImagePath, string petImagePath)
        {
            if (string.IsNullOrEmpty(existPetImagePath) && string.IsNullOrEmpty(petImagePath))
            {
                existPetImagePath = null;
            }
            else if (!string.IsNullOrEmpty(existPetImagePath) && string.IsNullOrEmpty(petImagePath))
            {
                // Пока удаление фото без удаления питомца не предусмотренно, возможна только замена
            }
            else if (string.IsNullOrEmpty(existPetImagePath) && !string.IsNullOrEmpty(petImagePath))
            {
                try
                {
                    SavePetImageFile(petImagePath);
                    existPetImagePath = petImagePath;
                }
                catch { }
            }
            else if (existPetImagePath != petImagePath)
            {
                try
                {
                    DeletePetImageFile(existPetImagePath);
                    existPetImagePath = null;
                    try
                    {
                        SavePetImageFile(petImagePath);
                        existPetImagePath = petImagePath;
                    }
                    catch
                    {
                        existPetImagePath = null;
                    }
                }
                catch { }
            }

            return existPetImagePath;
        }

        #endregion

    }
}