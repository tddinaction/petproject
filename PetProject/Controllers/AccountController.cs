﻿using System.Web.Mvc;
using PetProject.Models.NoSqlEntities;
using System.Web.Security;
using PetProject.Infrastructure;

namespace PetProject.Controllers
{
    [AllowAnonymous]
    public class AccountController : BaseController
    {
        IFormsAuthenticationProvider authProvider;
        public AccountController(IFormsAuthenticationProvider auth)
        {
            authProvider = auth;
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginData loginData, string returnUrl)
        {
            if (authProvider.Authenticate(loginData))
            {
                return Redirect(returnUrl ?? Url.Action("Index", "Home"));
            }
            else
            {
                ModelState.AddModelError("", "Incorrect login or password.");
                return View();
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }
    }
}