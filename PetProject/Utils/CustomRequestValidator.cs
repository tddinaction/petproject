﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Util;

namespace PetProject
{
    public class CustomRequestValidator : RequestValidator
    {
        protected override bool IsValidRequestString(HttpContext context, string value, RequestValidationSource requestValidationSource,
            string colectionKey, out int validationFailerIndex)
        {
            if (requestValidationSource == RequestValidationSource.Form)
            {
                if (value.Contains("''"))
                {
                    validationFailerIndex = value.IndexOf("''");
                    return false;
                }
                else if (value.Contains("--"))
                {
                    validationFailerIndex = value.IndexOf("--");
                    return false;
                }
            }

            return base.IsValidRequestString(context, value, requestValidationSource, colectionKey, out validationFailerIndex);
        }
    }
}