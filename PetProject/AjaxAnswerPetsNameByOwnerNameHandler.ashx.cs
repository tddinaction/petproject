﻿using PetProject.Models.Repository;
using PetProject.Models.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetProject
{
    /// <summary>
    /// Сводное описание для AjaxAnswerPetsNameByOwnerNameHandler
    /// </summary>
    public class AjaxAnswerPetsNameByOwnerNameHandler : IHttpHandler
    {
        public bool IsReusable => true;

        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;

            response.ContentType = "text/plain";

            string owner = context.Request.QueryString["OwnerName"];

            if (!string.IsNullOrEmpty(owner))
            {
                using (var db = new DB())
                {
                    Owner own = null;

                    try
                    {
                        own = db.InjectedContext.Owners.SingleOrDefault(m => m.Name == owner);
                    }
                    catch
                    {
                        own = db.InjectedContext.Owners.FirstOrDefault(m => m.Name == owner);
                    }

                    if (own != null)
                    {
                        List<Pet> pets = db.InjectedContext.GetAllPetsByOwnerId(own.OwnerId);
                        string answer = string.Empty;

                        if (pets.Count > 0)
                        {
                            foreach (var pet in pets)
                            {
                                answer += pet.Name + ", ";
                            }
                            answer = answer.Substring(0, answer.Length - 2);
                        }
                        else
                        {
                            answer = "there are no pets.";
                        }

                        // питомцы есть только в этом варианте
                        response.Write("Pets: " + answer);

                        int i = 1;
                    }
                    else
                    {
                        response.Write("No such owner.");
                    }
                }
            }
            else
            {
                response.Write("Invalid input.");
            }
        }
    }
}