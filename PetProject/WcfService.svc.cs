﻿using System.ServiceModel;
using System.Collections.Generic;
using PetProject.Controllers;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;

namespace PetProject
{
    [ServiceContract]
    public interface IWcfService
    {
        [OperationContract]
        IEnumerable<OwnerStatView> HomeIndex();
        [OperationContract]
        int HomeCreate(string name);
        [OperationContract]
        int HomeDelete(int oid);
        [OperationContract]
        IEnumerable<Pet> DetailIndex(int oid);
        [OperationContract]
        int DetailCreate(string name, int oid);
        [OperationContract]
        int DetailDelete(int pid, int oid);
    }

    public class WcfService : IWcfService
    {

        public IEnumerable<OwnerStatView> HomeIndex()
        {
            using (AjaxHomeController home = new AjaxHomeController())
            {
                return home.Index();
            }
        }

        public int HomeCreate(string name)
        {
            using (AjaxHomeController home = new AjaxHomeController())
            {
                return (int)home.Create(name).StatusCode;
            }
        }

        public int HomeDelete(int oid)
        {
            using (AjaxHomeController home = new AjaxHomeController())
            {
                return (int)home.Delete(oid).StatusCode;
            }
        }

        public IEnumerable<Pet> DetailIndex(int oid)
        {
            using (AjaxDetailController detail = new AjaxDetailController())
            {
                return detail.Index(oid);
            }
        }

        public int DetailCreate(string name, int oid)
        {
            using (AjaxDetailController detail = new AjaxDetailController())
            {
                return (int)detail.Create(name, oid).StatusCode;
            }
        }

        public int DetailDelete(int pid, int oid)
        {
            using (AjaxDetailController detail = new AjaxDetailController())
            {
                return (int)detail.Delete(pid, oid).StatusCode;
            }
        }
    }
}