﻿
$.validator.messages.required = /*"Это поле обязательно для заполнения."*/"This field is required.";
$.validator.messages.remote = /*"Пожалуйста, исправьте это поле."*/"Please correct this field.";
$.validator.messages.email = /*"Пожалуйста, введите корректный email-адрес."*/"Please enter a valid email.";
$.validator.messages.url = /*"Пожалуйста, введите корректный URL."*/"Please enter a valid URL.";
$.validator.messages.date = /*"Пожалуйста, введите корректное значение даты."*/"Please enter a valid date.";
$.validator.messages.dateISO = /*"Пожалуйста, введите корректное значение даты (ISO)."*/"Please enter a valid date (ISO).";
$.validator.messages.number = /*"Пожалуйста, введите корректное число."*/"Please enter a valid number.";
$.validator.messages.digits = /*"Пожалуйста, введите только цифры."*/"Please enter only digits.";
$.validator.messages.maxlength = $.validator.format(/*"Пожалуйста, введите не более {0} символов."*/"Please enter no more than {0} characters.");
$.validator.messages.minlength = $.validator.format(/*"Пожалуйста, введите более {0} символов."*/"Please enter more than {0} characters.");
$.validator.messages.rangelength = $.validator.format(/*"Пожалуйста, введите значение длиной от {0} до {1} символов."*/"Please enter a value from {0} to {1} characters long.");
$.validator.messages.range = $.validator.format(/*"Пожалуйста, введите значение от {0} до {1}."*/"Please enter a value from {0} to {1}.");
$.validator.messages.max = $.validator.format(/*"Пожалуйста, введите значение меньшее или равное {0}."*/"Please enter a value less than or equal to {0}.");
$.validator.messages.min = $.validator.format(/*"Пожалуйста, введите значение большее или равное {0}."*/"Please enter a value greater than or equal to {0}.");

$.ajaxSetup({
    async: false
});

$.validator.addMethod('validName', function (value) {

    //var allowed = "abcdefghijklmnopqrstuvwxyz" +
    //              "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
    //              "абвгдеёжзийклмнопрстуфхцчшщъыьэюя" +
    //              "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" +
    //              "1234567890" +
    //    " `~!@#$%^&*()_-=+[]{};:'\\|,./?\"";
    var allowed = "abcdefghijklmnopqrstuvwxyz" +
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
        "1234567890" +
        "- ";

    for (var i = 0; i < value.length; i++) {
        if (allowed.indexOf(value.charAt(i)) == -1) {
            return false;
        }
    }
    return true;
}, /*'Поле содержит некорректные символы'*/'Use only english characters, digits or hyphen');