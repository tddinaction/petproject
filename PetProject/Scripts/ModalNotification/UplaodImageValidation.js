﻿$('input[type="file"][max-size]').bind('change', function () {
    var fileSize = this.files[0].size;
    var maxSize = parseInt($(this).attr('max-size'), 10);

    var fileName = this.files[0].name;
    var dotPosition = fileName.lastIndexOf(".");
    var fileExt = fileName.substring(dotPosition);

    if (fileSize > maxSize) {
        $(this).val(null);
        $('#fileNameErrorMessage').text("Too big size of file (max size " + maxSize + " bytes)!");
    }
    else if (fileExt != '.jpg' && fileExt != '.png' && fileExt != '.jpeg') {
        $(this).val(null);
        $('#fileNameErrorMessage').text("Incorrect file format (use jpg, png or jpeg)!");
    }
    else {
        $('#fileNameErrorMessage').text("");
    }
    });