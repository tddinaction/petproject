﻿$(document).ready(function () {
    $.datepicker.setDefaults($.datepicker.regional['en']);
    $("#begining").datepicker({
        showOn: "button",
        buttonImage: "/Images/calendar.gif",
        showButtonPanel: true,
        firstDay: 1,
        dateFormat: "MM/dd/yy 00:00"
    });
});