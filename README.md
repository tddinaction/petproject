Pet project - example using ASP .NET MVC + WebForms + Web API + WCF (proxy) + Knockout.js + SQLite (Entity, Code First).

This site is intended for registration of pets who will subsequently be invited to exhibitions. This project began as 
a test task. There is minimal amount of pages, since the main aim is to test the sharing of technologies.
Nevertheless, in this project there are such interesting solutions:
- has a cart that can be used in online stores (send order info by email or save in file);
- has a model placed in a separate project, and implementations of interfaces that allow 
  testing without the use of a test database.
- has a good coverage unit tests;
- the interface is designed to work with all monitor resolutions, including mobile devices;
- has menu item History of existing pets with inforatiom about actoins of users.

WARNING: Requests to WebForms and WCF in this implementation do not require authentication,
to simplify the testing of technology sharing. I am using this version SQLite with Code First 
to simplify deployment on HYPER-V without internet access and the ability to demonstrate 
solutions without installation. For production I strongly recommend using MSSQLLocalDB instead,
because this version has problems with Cyrillic characters, the multiuser mode is not implemented
well enough, transactions are not supported, and the data adapter is not implemented.


