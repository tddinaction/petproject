﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetProject.Models.Abstract;
using System.Web.Mvc;
using PetProject.Models.NoSqlEntities;
using PetProject.Controllers;
using System.Collections.Generic;
using PetProject.Models.Entities;
using System.Linq;
using PetProject.Models.Repository;

namespace UnitTestADD
{
    [TestClass]
    public class HomeControllerTests
    {
        [TestMethod]
        public void Can_Index_Contains_Owners_And_Paginate()
        {
            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.GetOwnerStatView()).Returns(new List<OwnerStatView>
                { 
                new OwnerStatView { OwnerId = 1, Name = "Owner#1", Count = 0 },
                new OwnerStatView { OwnerId = 2, Name = "Owner#2", Count = 0 },
                new OwnerStatView { OwnerId = 3, Name = "Owner#3", Count = 0 },
                new OwnerStatView { OwnerId = 4, Name = "Owner#4", Count = 0 },
                new OwnerStatView { OwnerId = 5, Name = "Owner#5", Count = 0 }
            });

            HomeController controller = new HomeController(mock.Object);
            OwnerListViewModel result = ((OwnerListViewModel)((ViewResult)controller.Index(2)).Model);

            Assert.IsTrue(controller.PageSize == 3);
            Assert.IsTrue(result.Owners.Count() == 2);
            Assert.AreEqual(result.Owners.ToList()[0].Name, "Owner#4");
            Assert.AreEqual(result.Owners.ToList()[1].Name, "Owner#5");
        }

        [TestMethod]
        public void Can_Show_Create_Info_For_Owner()
        {
            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            HomeController controller = new HomeController(mock.Object);

            Owner owner = ((PartialViewResult)controller.Create()).Model as Owner;

            Assert.IsNotNull(owner);
        }

        [TestMethod]
        public void Can_Show_Edit_Info_For_Owner()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindOwner(oid)).Returns(new Owner { OwnerId = oid, Name = "Owner#1" });

            HomeController controller = new HomeController(mock.Object);

            Owner owner = ((PartialViewResult)controller.Edit(oid)).Model as Owner;

            Assert.AreEqual(1, owner.OwnerId);
        }

        [TestMethod]
        public void Cannot_Show_Edit_Info_For_Nonexistent_Owner()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Owner owner = null;
            mock.Setup(m => m.FindOwner(oid)).Returns(owner);

            HomeController controller = new HomeController(mock.Object);

            var result = controller.Edit(oid);

            Assert.IsNotInstanceOfType(result, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Can_Show_Delete_Info_For_Owner()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindOwner(oid)).Returns(new Owner { OwnerId = oid, Name = "Owner#1" });

            HomeController controller = new HomeController(mock.Object);

            Owner owner = ((PartialViewResult)controller.Delete(oid)).Model as Owner;

            Assert.AreEqual(1, owner.OwnerId);
        }

        [TestMethod]
        public void Cannot_Show_Delete_Info_For_Nonexistent_Owner()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Owner owner = null;
            mock.Setup(m => m.FindOwner(oid)).Returns(owner);

            HomeController controller = new HomeController(mock.Object);

            var result = controller.Delete(oid);

            Assert.IsNotInstanceOfType(result, typeof(PartialViewResult));
        }
    }
}
