﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PetProject.Models.Entities;
using PetProject.Models.NoSqlEntities;
using System.Collections.Generic;
using System.Linq;
using Moq;
using PetProject.Models.Abstract;
using PetProject.Controllers;
using System.Web.Mvc;
using System.Web;

namespace UnitTestADD
{
    [TestClass]
    public class CartControllerTests
    {
        [TestMethod]
        public void Can_Add_NewLines()
        {
            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1" };
            Pet pet2 = new Pet { PetId = 2, Name = "Питомец2" };

            Cart cart = new Cart();
            cart.AddItem(pet1, 1);
            cart.AddItem(pet2, 1);
            List<CartLine> results = cart.Lines.ToList();

            Assert.AreEqual(results.Count(), 2);
            Assert.AreEqual(results[0].Pet, pet1);
            Assert.AreEqual(results[1].Pet, pet2);
        }

        [TestMethod]
        public void Cannot_Add_Quantity_For_Existing_Lines()
        {
            // питомцы у нас пока уникальны

            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1" };
            Pet pet2 = new Pet { PetId = 2, Name = "Питомец2" };

            Cart cart = new Cart();
            cart.AddItem(pet1, 1);
            cart.AddItem(pet2, 1);
            cart.AddItem(pet1, 5);
            List<CartLine> results = cart.Lines.OrderBy(x => x.Pet.PetId).ToList();

            Assert.AreEqual(results.Count(), 2);
            Assert.AreEqual(results[0].Quantity, 1);
            Assert.AreEqual(results[1].Quantity, 1);
        }

        [TestMethod]
        public void Can_Remove_Line()
        {
            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1" };
            Pet pet2 = new Pet { PetId = 2, Name = "Питомец2" };
            Pet pet3 = new Pet { PetId = 3, Name = "Питомец3" };

            Cart cart = new Cart();
            cart.AddItem(pet1, 1);
            cart.AddItem(pet2, 4);
            cart.AddItem(pet3, 2);
            cart.AddItem(pet2, 1);

            cart.RemoveLine(pet2);

            Assert.AreEqual(cart.Lines.Where(x => x.Pet == pet2).Count(), 0);
            Assert.AreEqual(cart.Lines.Count(), 2);
        }

        [TestMethod]
        public void Calculate_Cart_Total()
        {
            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1", PriceExhibition = 34 };
            Pet pet2 = new Pet { PetId = 2, Name = "Питомец2", PriceExhibition = 54 };

            Cart cart = new Cart();
            cart.AddItem(pet1, 1);
            cart.AddItem(pet2, 1);
            decimal result = cart.ComputeTotalValue();

            Assert.AreEqual(result, 88);
        }

        [TestMethod]
        public void Can_Clear_Content()
        {
            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1" };
            Pet pet2 = new Pet { PetId = 2, Name = "Питомец2" };

            Cart cart = new Cart();
            cart.AddItem(pet1, 1);
            cart.AddItem(pet2, 1);

            cart.Clear();

            Assert.AreEqual(cart.Lines.Count(), 0);
        }

        [TestMethod]
        public void Can_Add_To_Cart()
        {
            Mock<IDBRepository> mockDB = new Mock<IDBRepository>();
            mockDB.Setup(m => m.Pets).Returns(new List<Pet>
            {
                new Pet { PetId = 1, Name = "Питомец1" }
            }.AsQueryable());

            Mock<IOrderProcessor> mockOrder = new Mock<IOrderProcessor>();

            Cart cart = new Cart();
            CartController controller = new CartController(mockDB.Object, mockOrder.Object);

            controller.AddToCart(cart, 1, null);

            Assert.AreEqual(cart.Lines.Count(), 1);
            Assert.AreEqual(cart.Lines.ToList()[0].Pet.PetId, 1);
        }

        [TestMethod]
        public void Adding_Pet_To_Cart_Goes_To_CartScreen()
        {
            Mock<IDBRepository> mockDB = new Mock<IDBRepository>();
            mockDB.Setup(m => m.Pets).Returns(new List<Pet>
            {
                new Pet { PetId = 1, Name = "Питомец1" }
            }.AsQueryable());

            Mock<IOrderProcessor> mockOrder = new Mock<IOrderProcessor>();

            Cart cart = new Cart();
            CartController controller = new CartController(mockDB.Object, mockOrder.Object);

            RedirectToRouteResult result = controller.AddToCart(cart, 1, "myUrl");

            Assert.AreEqual(result.RouteValues["action"], "Index");
            Assert.AreEqual(result.RouteValues["returnUrl"], "myUrl");
        }

        [TestMethod]
        public void Can_View_Cart_Content()
        {
            Cart cart = new Cart();
            CartController target = new CartController(null, null);

            CartIndexViewModel result = (CartIndexViewModel)target.Index(cart, "myUrl").ViewData.Model;

            Assert.AreSame(result.Cart, cart);
            Assert.AreEqual(result.returnUrl, "myUrl");
        }

        [TestMethod]
        public void Cannot_Checkout_Empty_Chart()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();
            ShippingDetails shippingDetails = new ShippingDetails();
            CartController controller = new CartController(null, mock.Object);

            ViewResult result = controller.Checkout(cart, shippingDetails);

            mock.Verify(m => m.ProcessorOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never);

            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Cannot_Checkout_Invalid_Chart()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();

            ShippingDetails shippingDetails = new ShippingDetails();
            CartController controller = new CartController(null, mock.Object);

            // 0 - несуществующий PetId
            controller.AddToCart(cart, 0, null);

            ViewResult result = controller.Checkout(cart, shippingDetails);

            mock.Verify(m => m.ProcessorOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Never);

            Assert.AreEqual("", result.ViewName);
            Assert.AreEqual(false, result.ViewData.ModelState.IsValid);
        }

        [TestMethod]
        public void Can_Checkout_And_Submit_Order()
        {
            Mock<IOrderProcessor> mock = new Mock<IOrderProcessor>();
            Cart cart = new Cart();

            Pet pet1 = new Pet { PetId = 1, Name = "Питомец1" };
            cart.AddItem(pet1, 1);

            ShippingDetails shippingDetails = new ShippingDetails();
            CartController controller = new CartController(null, mock.Object);

            ViewResult result = controller.Checkout(cart, shippingDetails);

            mock.Verify(m => m.ProcessorOrder(It.IsAny<Cart>(), It.IsAny<ShippingDetails>()), Times.Once);

            Assert.AreEqual("Complete", result.ViewName);
            Assert.AreEqual(true, result.ViewData.ModelState.IsValid);
        }
    }
}
