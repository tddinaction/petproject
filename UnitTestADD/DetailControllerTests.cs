﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetProject.Models.Abstract;
using System.Web.Mvc;
using PetProject.Models.NoSqlEntities;
using PetProject.Controllers;
using System.Collections.Generic;
using PetProject.Models.Entities;
using System.Linq;

namespace UnitTestADD
{
    [TestClass]
    public class DetailControllerTests
    {
        [TestMethod]
        public void Can_Index_Contains_All_Pets_By_OwnerId()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.GetAllPetsByOwnerId(oid)).Returns(new List<Pet>
                {
                new Pet { PetId = 1, Name = "Pet#1", OwnerId = oid },
                new Pet { PetId = 2, Name = "Pet#2", OwnerId = oid },
                new Pet { PetId = 3, Name = "Pet#3", OwnerId = oid },
            });
            mock.Setup(m => m.FindOwner(oid)).Returns(new Owner { OwnerId = 1, Name = "Owner#1" });

            DetailController controller = new DetailController(mock.Object);
            List<Pet> result = ((IEnumerable<Pet>)((ViewResult)controller.Index(oid)).Model).ToList();

            Assert.IsTrue(result.Count() == 3);
            Assert.AreEqual(result[0].Name, "Pet#1");
            Assert.AreEqual(result[1].Name, "Pet#2");
            Assert.AreEqual(result[2].Name, "Pet#3");
        }

        [TestMethod]
        public void Can_Show_PetSummary_Info_For_Pet()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindPet(pid)).Returns(new Pet { PetId = pid, Name = "Pet#1", OwnerId = 1 });

            DetailController controller = new DetailController(mock.Object);

            Pet pet = ((ViewResult)controller.PetSummary(pid, "someUrl")).Model as Pet;

            Assert.IsNotNull(pet);
            Assert.AreEqual(pet.OwnerId, pid);
        }

        [TestMethod]
        public void Cannot_Show_PetSummary_Info_For_Pet_For_Nonexistent_Owner()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Pet pet = null;
            mock.Setup(m => m.FindPet(pid)).Returns(pet);

            DetailController controller = new DetailController(mock.Object);

            var result = controller.PetSummary(pid, "someUrl");

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Cannot_Show_PetSummary_Info_For_Pet_For_Nonexistent_Return_URL()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindPet(pid)).Returns(new Pet { PetId = pid, Name = "Pet#1", OwnerId = 1 });

            DetailController controller = new DetailController(mock.Object);

            var result = controller.PetSummary(pid, null);

            Assert.IsNotInstanceOfType(result, typeof(ViewResult));
        }

        [TestMethod]
        public void Can_Show_Create_Info_For_Pet()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindOwner(oid)).Returns(new Owner { OwnerId = oid, Name = "Owner#1" });

            DetailController controller = new DetailController(mock.Object);

            Pet pet = ((PartialViewResult)controller.Create(oid)).Model as Pet;

            Assert.IsNotNull(pet);
            Assert.AreEqual(pet.OwnerId, oid);
        }

        [TestMethod]
        public void Cannot_Show_Create_Info_For_Pet_For_Nonexistent_Owner()
        {
            int oid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Owner owner = null;
            mock.Setup(m => m.FindOwner(oid)).Returns(owner);

            DetailController controller = new DetailController(mock.Object);

            var result = controller.Create(oid);

            Assert.IsNotInstanceOfType(result, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Can_Show_Edit_Info_For_Pet()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindPet(pid)).Returns(new Pet { PetId = pid, Name = "Pet#1", OwnerId = 1 });

            DetailController controller = new DetailController(mock.Object);

            Pet pet = ((PartialViewResult)controller.Edit(pid)).Model as Pet;

            Assert.AreEqual(pet.PetId, pid);
        }

        [TestMethod]
        public void Cannot_Show_Edit_Info_For_Nonexistent_Pet()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Pet pet = null;
            mock.Setup(m => m.FindPet(pid)).Returns(pet);

            DetailController controller = new DetailController(mock.Object);

            var result = controller.Edit(pid);

            Assert.IsNotInstanceOfType(result, typeof(PartialViewResult));
        }

        [TestMethod]
        public void Can_Show_Delete_Info_For_Pet()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.FindPet(pid)).Returns(new Pet { PetId = pid, Name = "Pet#1", OwnerId = 1 });

            DetailController controller = new DetailController(mock.Object);

            Pet pet = ((PartialViewResult)controller.Delete(pid)).Model as Pet;

            Assert.AreEqual(pet.PetId, pid);
        }

        [TestMethod]
        public void Cannot_Show_Delete_Info_For_Nonexistent_Pet()
        {
            int pid = 1;

            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            Pet pet = null;
            mock.Setup(m => m.FindPet(pid)).Returns(pet);

            DetailController controller = new DetailController(mock.Object);

            var result = controller.Delete(pid);

            Assert.IsNotInstanceOfType(result, typeof(PartialViewResult));
        }
    }
}