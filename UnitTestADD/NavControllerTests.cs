﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PetProject.Models.Abstract;
using System.Collections.Generic;
using PetProject.Models.Entities;
using PetProject.Controllers;

namespace UnitTestADD
{
    [TestClass]
    public class NavControllerTests
    {
        [TestMethod]
        public void Can_Create_OwnerMenu_And_Select_Item()
        {
            Mock<IDBRepository> mock = new Mock<IDBRepository>();
            mock.Setup(m => m.Owners).Returns(new List<Owner> {
                new Owner { OwnerId = 1, Name = "Owner#1" },
                new Owner { OwnerId = 2, Name = "Owner#2" },
                new Owner { OwnerId = 3, Name = "Owner#3" }
            });

            int? choise = 2;
            NavController target = new NavController(mock.Object);
            List<Owner> itemsResult = (List<Owner>)target.OwnerMenu(choise).Model;
            int? seletedResult = (int?)target.OwnerMenu(choise).ViewBag.SelectedOnwerId;

            Assert.AreEqual(itemsResult.Count, 3);
            Assert.AreEqual(itemsResult[0].Name, "Owner#1");
            Assert.AreEqual(itemsResult[1].Name, "Owner#2");
            Assert.AreEqual(itemsResult[2].Name, "Owner#3");
            Assert.AreEqual(seletedResult, choise);
        }
    }
}
