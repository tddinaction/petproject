﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Web.Mvc;
using PetProject.Infrastructure;
using PetProject.Models.NoSqlEntities;
using PetProject.Controllers;

namespace UnitTestADD
{
    [TestClass]
    public class AccountControllerTests
    {
        [TestMethod]
        public void Can_Login_With_Valid_Credentials()
        {
            LoginData loginData = new LoginData { UserName = "admin", Password = "secret" };

            Mock<IFormsAuthenticationProvider> mock = new Mock<IFormsAuthenticationProvider>();
            mock.Setup(m => m.Authenticate(loginData)).Returns(true);

            AccountController target = new AccountController(mock.Object);
            ActionResult result = target.Login(loginData, "/Home");

            Assert.IsInstanceOfType(result, typeof(RedirectResult));
            Assert.AreEqual("/Home", ((RedirectResult)result).Url);
        }

        [TestMethod]
        public void Cannot_Login_With_Invalid_Credentials()
        {
            LoginData loginData = new LoginData { UserName = "admin", Password = "badpass" };

            Mock<IFormsAuthenticationProvider> mock = new Mock<IFormsAuthenticationProvider>();
            mock.Setup(m => m.Authenticate(loginData)).Returns(false);

            AccountController target = new AccountController(mock.Object);
            ActionResult result = target.Login(loginData, "/Home");

            Assert.IsInstanceOfType(result, typeof(ViewResult));
            Assert.IsFalse(((ViewResult)result).ViewData.ModelState.IsValid);
        }
    }
}